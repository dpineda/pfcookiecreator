var webpack = require('webpack'),
	htmlWebpackPlugin = require('html-webpack-plugin'),
	libDir = __dirname + '/src/js/lib/',
	appDir = __dirname + '/src/js/application/';

module.exports = {
	context: __dirname + '/src/js',
	devtool: 'source-map',
	entry: {
		main: './application/main.js'
	},
	output: {
		path: __dirname + '/dist/js',
		publicPath: '/dist/js',
		filename: '[name].[hash].bundle.js',
		chunkFilename: '[id].[hash].bundle.js'
	},
	resolve: {
		alias: {
			jquery: libDir + 'jquery-2.1.4.js',
			underscore: libDir + 'underscore.js',
			backbone: libDir + 'backbone.js',
			marionette: libDir + 'backbone.marionette.js',
			mustache: libDir + 'mustache.js',
			models: appDir + 'models',
			views: appDir + 'views',
			controllers: appDir + 'controllers',
			templates: appDir + 'templates'
		}
	},
	module: {
		loaders: [{
			test: /\.mustache$/,
			loader: 'mustache?minify'
		}]
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			compress: { warnings: false },
			comments: false
		}),
		new htmlWebpackPlugin({
			template: __dirname  + '/index.html',
			filename: __dirname + '/index.html',
			inject: 'body'
		})
	]
};