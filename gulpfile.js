/*globals module*/
var gulp = require('gulp'),
	del = require('del'),
	eslint = require('gulp-eslint'),
	gutil = require('gulp-util'),
	webpack = require('webpack');

(function (gulp) {
	'use strict';

	gulp.task('default', function () {
		console.log('hello world');
	});

	gulp.task('eslint', function () {
		return gulp.src(['src/js/**/*.js', '!src/js/lib/**/*.js'])
			.pipe(eslint({
				useEslintrc: true
			}))
			.pipe(eslint.format())
			//@TODO: only fail for prod builds
			.pipe(eslint.failAfterError());
	});

	gulp.task('clean', function () {
		return del([
			'dist/*'
		]);
	});

	gulp.task('webpack', function (callback) {
		var webpackConfig = Object.create(require('./webpack.config.js'));
		webpack(webpackConfig, function(err, stats) {
			if (err) {
				throw new gutil.PluginError("webpack", err);
			}
			gutil.log("[webpack]", stats.toString());
			callback();
		});
	});

	gulp.task('dist', ['clean', 'webpack']);
})(gulp);
