/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/rootView'
], function ($, _, Backbone, Marionette, RootView) {
	'use strict';
	var App = Marionette.Application.extend({
		rootView: null,
		onStart: function () {
			this.rootView = new RootView();
			this.rootView.render();
		}
	}),
	mainApp;

	//Override Marionette default renderer to use Mustache to render templates
	Backbone.Marionette.Renderer.render = function (template, data) {
		return template(data);
	};

	//Fetch the cookieData (mocked locally for testing purposes)
	mainApp = new App();
	mainApp.start();

	return mainApp;
});
