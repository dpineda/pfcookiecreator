/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/cookieIngredients',
	'templates/cookieToppings.mustache'
], function ($, _, Backbone, Marionette, CookieIngredientsView, cookieToppingsTemplate) {
	'use strict';

	var cookieToppingsView = Marionette.LayoutView.extend({
		cookie: null,
		cookieToppings: null,
		template: cookieToppingsTemplate,

		regions: {
			candyPanel: '#candyPanel',
			fruitPanel: '#fruitPanel',
			nutsPanel: '#nutsPanel',
			spicesPanel: '#spicesPanel'
		},

		ui: {
			tab: '.js_tab',
			candyPanel: '#candyPanel',
			fruitPanel: '#fruitPanel',
			nutsPanel: '#nutsPanel',
			spicesPanel: '#spicesPanel',
			previousButton: '.js_previousButton',
			nextButton: '.js_nextButton'
		},

		events: {
			'click @ui.tab': 'onTabClick',
			'click @ui.nextButton': 'onClickNext'
		},

		triggers: {
			'click @ui.previousButton': 'cookieToppings:previous'
		},

		childEvents: {
			'ingredient:select' : 'onCookieIngredientSelect'
		},

		initialize: function (options) {
			this.cookie = options.cookie;
			this.listenTo(this.cookie.toppings, 'update', this.onFillingsUpdate);
			this.listenTo(this.cookie.toppings, 'reset', this.onFillingsReset);
			this.cookieToppings = options.cookieToppings;
		},

		onRender: function () {
			var candyCollection = new Backbone.Collection(this.cookieToppings.candy),
				fruitCollection = new Backbone.Collection(this.cookieToppings.fruit),
				nutsCollection = new Backbone.Collection(this.cookieToppings.nuts),
				spicesCollection = new Backbone.Collection(this.cookieToppings.spices),
				candyView = new CookieIngredientsView({collection: candyCollection}),
				fruitView = new CookieIngredientsView({collection: fruitCollection}),
				nutsView = new CookieIngredientsView({collection: nutsCollection}),
				spicesView = new CookieIngredientsView({collection: spicesCollection});

			this.showChildView('candyPanel', candyView);
			this.showChildView('fruitPanel', fruitView);
			this.showChildView('nutsPanel', nutsView);
			this.showChildView('spicesPanel', spicesView);
		},

		onTabClick: function (e) {
			var target = $(e.target).closest('li'),
				targetPanel = target.data('panel');
			e.preventDefault();
			this.$el.find('.ui-tabs-panel').addClass('hide');
			this.ui[targetPanel].removeClass('hide');
			this.$el.find('.ui-state-active').removeClass('ui-state-active');
			target.addClass('ui-state-active');
		},

		onCookieIngredientSelect: function (childView) {
			var cookieIngredient = childView.ui.cookieIngredient,
				ingredient = childView.model.clone();
			if (cookieIngredient.hasClass('selected')) {
				cookieIngredient.removeClass('selected');
				this.cookie.toppings.remove(ingredient);
			} else {
				if (this.cookie.toppings.length < 3) {
					childView.ui.cookieIngredient.addClass('selected');
					this.cookie.toppings.push(ingredient);
				}
			}
		},

		onFillingsUpdate: function (fillings) {
			if (fillings.length > 0) {
				this.ui.nextButton.removeClass('disabled');
			} else {
				this.ui.nextButton.addClass('disabled');
			}
		},

		onFillingsReset: function () {
			this.$el.find('.js_cookieIngredient').removeClass('selected');
			this.ui.nextButton.addClass('disabled');
		},

		onClickNext: function (e) {
			e.preventDefault();
			if (this.cookie.toppings.length > 0) {
				this.triggerMethod('cookieToppings:next');
			}
		}
	});

	return cookieToppingsView;
});
