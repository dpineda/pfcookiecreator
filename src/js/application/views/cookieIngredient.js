/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'templates/cookieIngredient.mustache'
], function ($, _, Backbone, Marionette, cookieIngredientTemplate) {
	'use strict';
	var CookieIngredientView = Marionette.ItemView.extend({
		template: cookieIngredientTemplate,
		tagName: 'li',
		ui: {
			cookieIngredient: '.js_cookieIngredient'
		},
		triggers: {
			'click @ui.cookieIngredient': 'ingredient:select'
		},

		className: function () {
			return this.model.get('id');
		}
	});

	return CookieIngredientView;
});
