/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'templates/cookieType.mustache'
], function ($, _, Backbone, Marionette, cookieTypeTemplate) {
	'use strict';
	var CookieTypeView = Marionette.ItemView.extend({
		template: cookieTypeTemplate,
		tagName: 'li',

		ui: {
			cookieType: '.js_cookieType'
		},

		triggers: {
			'click @ui.cookieType': 'cookieType:select'
		},

		className: function () {
			return this.model.get('id');
		}

	});

	return CookieTypeView;
});
