/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/cookieIngredients',
	'templates/cookieFillings.mustache'
], function ($, _, Backbone, Marionette, CookieIngredientsView, cookieFillingsTemplate) {
	'use strict';

	var cookieFillingsView = Marionette.LayoutView.extend({
		cookie: null,
		cookieFillings: null,
		template: cookieFillingsTemplate,

		regions: {
			chocolatePanel: '#chocolatePanel',
			fruitPanel: '#fruitPanel',
			otherPanel: '#otherPanel'
		},

		ui: {
			tab: '.js_tab',
			chocolatePanel : '#chocolatePanel',
			fruitPanel: '#fruitPanel',
			otherPanel: '#otherPanel',
			previousButton: '.js_previousButton',
			nextButton: '.js_nextButton'
		},

		events: {
			'click @ui.tab': 'onTabClick',
			'click @ui.nextButton': 'onClickNext'
		},

		triggers: {
			'click @ui.previousButton': 'cookieFillings:previous'
		},

		childEvents: {
			'ingredient:select' : 'onCookieIngredientSelect'
		},

		initialize: function (options) {
			this.cookie = options.cookie;
			this.listenTo(this.cookie.fillings, 'update', this.onFillingsUpdate);
			this.listenTo(this.cookie.fillings, 'reset', this.onFillingsReset);
			this.cookieFillings = options.cookieFillings;
		},

		onRender: function () {
			var chocolateCollection = new Backbone.Collection(this.cookieFillings.chocolate),
				fruitCollection = new Backbone.Collection(this.cookieFillings.fruit),
				otherCollection = new Backbone.Collection(this.cookieFillings.other),
				chocolateView = new CookieIngredientsView({collection: chocolateCollection}),
				fruitView = new CookieIngredientsView({collection: fruitCollection}),
				otherView = new CookieIngredientsView({collection: otherCollection});

			this.showChildView('chocolatePanel', chocolateView);
			this.showChildView('fruitPanel', fruitView);
			this.showChildView('otherPanel', otherView);
		},

		onTabClick: function (e) {
			var target = $(e.target).closest('li'),
				targetPanel = target.data('panel');
			e.preventDefault();
			this.$el.find('.ui-tabs-panel').addClass('hide');
			this.ui[targetPanel].removeClass('hide');
			this.$el.find('.ui-state-active').removeClass('ui-state-active');
			target.addClass('ui-state-active');
		},

		onCookieIngredientSelect: function (childView) {
			var cookieIngredient = childView.ui.cookieIngredient,
				ingredient = childView.model.clone();
			if (cookieIngredient.hasClass('selected')) {
				cookieIngredient.removeClass('selected');
				this.cookie.fillings.remove(ingredient);
			} else {
				if (this.cookie.fillings.length < 2) {
					childView.ui.cookieIngredient.addClass('selected');
					this.cookie.fillings.push(ingredient);
				}
			}
		},

		onFillingsUpdate: function (fillings) {
			if (fillings.length > 0) {
				this.ui.nextButton.removeClass('disabled');
			} else {
				this.ui.nextButton.addClass('disabled');
			}
		},

		onFillingsReset: function () {
			this.$el.find('.js_cookieIngredient').removeClass('selected');
			this.ui.nextButton.addClass('disabled');
		},

		onClickNext: function (e) {
			e.preventDefault();
			if (this.cookie.fillings.length > 0) {
				this.triggerMethod('cookieFillings:next');
			}
		}
	});

	return cookieFillingsView;
});
