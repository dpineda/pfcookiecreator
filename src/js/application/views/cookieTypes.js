/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/cookieType',
	'templates/cookieTypes.mustache'
], function ($, _, Backbone, Marionette, CookieTypeView, cookieTypesTemplate) {
	'use strict';
	var CookieTypesView = Marionette.CompositeView.extend({
		cookie: null,
		template: cookieTypesTemplate,
		childView: CookieTypeView,
		childViewContainer: '.js_cookieTypesView',
		childEvents: {
			'cookieType:select' : 'onCookieTypeSelect'
		},

		ui: {
			nextButton: '.js_nextButton'
		},

		events: {
			'click @ui.nextButton': 'onClickNext'
		},

		initialize: function (options) {
			this.cookie = options.cookie;
			this.listenTo(this.cookie, 'change:cookieType', this.onCookieTypeChange);
		},

		onCookieTypeSelect: function (childView) {
			var cookieType = childView.model.get('id'),
				cookieName = childView.model.get('name');
			this.cookie.set({'cookieType': cookieType, 'cookieName': cookieName});
			this.$childViewContainer.find('li').removeClass('selected');
			this.$childViewContainer.find('li.' + cookieType).addClass('selected');
		},

		onCookieTypeChange: function () {
			this.cookie.flavor.clear();
			this.cookie.fillings.reset(null);
			this.cookie.toppings.reset(null);
			this.ui.nextButton.removeClass('disabled');
		},

		onClickNext: function (e) {
			e.preventDefault();
			if (this.cookie.get('cookieType')) {
				this.triggerMethod('cookieTypes:next');
			}
		}

	});

	return CookieTypesView;
});
