/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette'
], function ($, _, Backbone, Marionette) {
	'use strict';

	var CookieNameView = Marionette.ItemView.extend({
		el: '#inspirationForm',
		template: false,

		ui: {
			name: 'input[name="name"]',
			inspiration: 'textarea[name="inspiration"]',
			nextBtn: '.btnNext'
		},

		events: {
			'click @ui.nextBtn': 'onFormSubmit'
		},

		onFormSubmit: function (e) {
			e.preventDefault();
			this.model.set({
				cookieName: this.ui.name.val(),
				cookieInspiration: this.ui.inspiration.val()
			});
			this.trigger('wizard:nextStep');
		}
	});

	return CookieNameView;
});
