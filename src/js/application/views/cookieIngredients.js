/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/cookieIngredient',
	'templates/cookieIngredients.mustache'
], function ($, _, Backbone, Marionette, CookieIngredientView, cookieIngredientsTemplate) {
	'use strict';

	var cookieIngredientsView = Marionette.CompositeView.extend({
		cookie: null,
		template: cookieIngredientsTemplate,
		childView: CookieIngredientView,
		childViewContainer: '.js_cookieFlavorsView'

	});

	return cookieIngredientsView;
});
