/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'views/cookieIngredient',
	'templates/cookieFlavors.mustache'
], function ($, _, Backbone, Marionette, CookieIngredientView, cookieFlavorsTemplate) {
	'use strict';

	var CookieFlavorsView = Marionette.CompositeView.extend({
		cookie: null,
		template: cookieFlavorsTemplate,
		childView: CookieIngredientView,
		childViewContainer: '.js_cookieFlavorsView',
		childEvents: {
			'ingredient:select': 'onCookieFlavorSelect'
		},
		ui: {
			previousButton: '.js_previousButton',
			nextButton: '.js_nextButton'
		},

		events: {
			'click @ui.nextButton': 'onClickNext'
		},

		triggers: {
			'click @ui.previousButton': 'cookieFlavors:previous'
		},

		initialize: function (options) {
			this.cookie = options.cookie;
			this.listenTo(this.cookie.flavor, 'change', this.onFlavorChange);
		},

		onCookieFlavorSelect: function (childView) {
			var cookieIngredient = childView.ui.cookieIngredient,
				ingredient = childView.model.clone();
			if (this.cookie.flavor.isEmpty()) {
				childView.ui.cookieIngredient.addClass('selected');
				this.cookie.flavor.set(ingredient.attributes);
			} else {
				if (cookieIngredient.hasClass('selected')) {
					cookieIngredient.removeClass('selected');
					this.cookie.flavor.clear();
				}
			}
		},

		onFlavorChange: function (flavor) {
			if (!flavor.isEmpty()) {
				this.ui.nextButton.removeClass('disabled');
			} else {
				this.ui.nextButton.addClass('disabled');
			}
		},

		onClickNext: function (e) {
			e.preventDefault();
			if (!this.cookie.flavor.isEmpty()) {
				this.triggerMethod('cookieFlavor:next');
			}
		}
	});

	return CookieFlavorsView;
});
