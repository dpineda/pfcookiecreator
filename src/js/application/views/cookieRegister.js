/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette'
], function ($, _, Backbone, Marionette) {
	'use strict';
	//Taken from http://emailregex.com/ (might be a bit excessive)
	var emailRegex = /^([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}$/igm,
		CookieRegisterView = Marionette.ItemView.extend({
			el: '.js_cookieRegisterView',
			template: false,

			ui: {
				firstName: 'input[name="fname"]',
				lastName: 'input[name="lname"]',
				email: 'input[name="email"]',
				email2: 'input[name="email2"]',
				password: 'input[name="password"]',
				month: 'select[name="month"]',
				day: 'select[name="day"]',
				year: 'select[name="year"]',
				zip: 'input[name="zip"]',
				rules: 'input[name="rules"]',
				newsletter: 'input[name="newsletter"]',
				nextBtn: '.btnEnter',
				loginBtn: '.btnSubmit',
				toggleLogin: '.toggle-login',
				toggleRegister: '.toggle-register',
				loginForm: '.login',
				registerForm: '.register'
			},

			events: {
				'click @ui.nextBtn': 'onFormSubmit',
				'clcik @ui.loginBtn': 'onLoginSubmit',
				'click @ui.toggleLogin': 'onToggleLogin',
				'click @ui.toggleRegister': 'onToggleRegister'
			},

			onFormSubmit: function (e) {
				e.preventDefault();
				this.model.set({
					firstName: this.ui.firstName.val(),
					lastName: this.ui.lastName.val(),
					email: this.ui.email.val(),
					email2: this.ui.email2.val(),
					password: this.ui.password.val(),
					month: this.ui.month.val(),
					day: this.ui.day.val(),
					year: this.ui.year.val(),
					zip: this.ui.zip.val(),
					rules: this.ui.rules.is(':checked'),
					newsletter: this.ui.newsletter.is(':checked')
				});
				if (this.validateForm()) {
					this.ui.loginForm.hide();
					this.ui.registerForm.hide();
					this.trigger('wizard:nextStep');
				}
			},

			onLoginSubmit: function () {
				//TODO: implement this
			},

			validateForm: function () {
				var dateString = this.model.get('day') + '/' + this.model.get('month') + '/' + this.model.get('year'),
					formIsValid = true;
				this.$el.find('input').removeClass('error');
				this.$el.find('select').removeClass('error');
				this.$el.find('.errorMessage').hide();


				if (this.model.get('firstName').length === 0) {
					this.toggleFormError(this.ui.firstName, 'Required');
					formIsValid = false;
				}
				if (this.model.get('lastName').length === 0) {
					this.toggleFormError(this.ui.lastName, 'Required');
					formIsValid = false;
				}
				if (this.model.get('email2').length === 0) {
					this.toggleFormError(this.ui.email2, 'Required');
					formIsValid = false;
				}
				if (this.model.get('password').length === 0) {
					this.toggleFormError(this.ui.password, 'Required');
					formIsValid = false;
				}
				if (this.model.get('zip').length === 0) {
					this.toggleFormError(this.ui.zip, 'Required');
					formIsValid = false;
				}
				if (!this.model.get('rules')) {
					this.toggleFormError(this.ui.rules, 'Required');
					formIsValid = false;
				}
				if (this.model.get('email') !== this.model.get('email2')) {
					this.toggleFormError(this.ui.email, 'Emails don\'t match');
					this.toggleFormError(this.ui.email2, 'Emails don\'t match');
					formIsValid = false;
				}
				if (this.model.get('password').length < 9) {
					this.toggleFormError(this.ui.password, 'Must be 9 characters or more');
					formIsValid = false;
				}
				if (this.model.get('email').match(emailRegex) === null) {
					this.toggleFormError(this.ui.email, 'Must be a valid email pattern like test@email.com');
					formIsValid = false;
				}

				if (!this.validateDateString(dateString)) {
					this.toggleFormError(this.$el.find('.dateWrapper'), 'Must be a valid date');
					formIsValid = false;
				}

				return formIsValid;
			},

			toggleFormError: function (element, message) {
				var errorEl = element.siblings('.errorMessage');
				element.addClass('error');
				if (errorEl.length === 0) {
					if (element.prop('type') === 'checkbox') {
						$('<span class="errorMessage">' + message + '</span>').insertAfter(element.siblings('label'));
					} else {
						$('<span class="errorMessage">' + message + '</span>').insertAfter(element);
					}
				} else {
					$(errorEl).html(message);
				}
				$(errorEl).show();
			},

			validateDateString: function (dateString) {
				var parts = dateString.split('/'),
					day = parseInt(parts[1], 10),
					month = parseInt(parts[0], 10),
					year = parseInt(parts[2], 10),
					monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

				if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString)) {
					return false;
				}
				// Adjust for leap years
				if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
					monthLength[1] = 29;
				}
				// Check the range of the day
				return day > 0 && day <= monthLength[month - 1];
			},

			onToggleLogin: function (e) {
				e.preventDefault();
				this.ui.registerForm.slideUp(400, function () {
					this.ui.loginForm.slideDown();
				}.bind(this));
			},

			onToggleRegister: function (e) {
				e.preventDefault();
				this.ui.loginForm.slideUp(400, function () {
					this.ui.registerForm.slideDown();
				}.bind(this));
			}

		});
	return CookieRegisterView;
});
