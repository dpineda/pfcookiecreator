/*globals define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',

	'models/cookie',
	'views/cookieTypes',
	'views/cookieFillings',
	'views/cookieFlavors',
	'views/cookieToppings',
	'views/cookieResult'
], function ($, _, Backbone, Marionette, Cookie, CookieTypesView, CookieFillingsView, CookieFlavorsView, CookieToppingsView, CookieResultView) {
	'use strict';
	var CookieWizardView = Marionette.LayoutView.extend({
		template: false,
		el: '#cookieWizard',

		regions: {
			cookieType: '.js_cookieType',
			cookieFlavor: '.js_cookieFlavor',
			cookieFillings: '.js_cookieFillings',
			cookieToppings: '.js_cookieToppings',
			cookieResult: '.js_cookieResult'
		},

		childEvents: {
			'cookieTypes:next': 'onCookieTypesClickNext',
			'cookieFlavor:next': 'onCookieFlavorClickNext',
			'cookieFillings:next': 'onCookieFillingsClickNext',
			'cookieToppings:next': 'onCookieToppingsClickNext',

			'cookieFlavors:previous': 'onCookieFlavorClickPrevious',
			'cookieFillings:previous': 'onCookieFillingsClickPrevious',
			'cookieToppings:previous': 'onCookieToppingsClickPrevious',
			'cookieResult:previous': 'onCookieResultClickPrevious',

			'cookieResult:done': 'onCookieResultClickDone'
		},

		initialize: function (options) {
			this.cookieData = options.cookieData;
			this.cookie = new Cookie();
			this.cookie.flavor = new Backbone.Model();
			this.cookie.fillings = new Backbone.Collection();
			this.cookie.toppings = new Backbone.Collection();

			// UTILITIES TO DEBUG COOKIE CHANGES THROUGH THE WIZARD
			// this.listenTo(this.cookie, 'change', function (){
			// 	window.console.log('COOOKIE TYPE');
			// 	window.console.log(this.cookie.attributes);
			// }.bind(this));

			// this.listenTo(this.cookie.flavor, 'change', function (){
			// 	window.console.log('COOOKIE FLAVOR');
			// 	window.console.log(this.cookie.flavor.attributes);
			// }.bind(this));

			// this.listenTo(this.cookie.fillings, 'update', function (){
			// 	window.console.log('COOOKIE FILLINGS');
			// 	_.each(this.cookie.fillings.models, function (model) {
			// 		window.console.log(model.attributes);
			// 	});
			// }.bind(this));

			// this.listenTo(this.cookie.toppings, 'update', function (){
			// 	window.console.log('COOOKIE TOPPINGS');
			// 	_.each(this.cookie.fillings.models, function (model) {
			// 		window.console.log(model.attributes);
			// 	});
			// }.bind(this));
		},

		onCookieTypesClickNext: function () {
			var cookieFlavors = this.cookieData.findWhere({id: this.cookie.get('cookieType')}).attributes.flavors,
				cookieFlavorsCollection = new Backbone.Collection(cookieFlavors),
				cookieFlavorsView = new CookieFlavorsView({cookie: this.cookie, collection: cookieFlavorsCollection});

			this.showChildView('cookieFlavor', cookieFlavorsView);
			this.getRegion('cookieFlavor').$el.show();
			this.getRegion('cookieType').$el.hide();

			this.scrollWizardInView();
		},

		onCookieFlavorClickNext: function () {
			var cookieFillings = this.cookieData.findWhere({id: this.cookie.get('cookieType')}).attributes.fillings,
				cookieFillingsView = new CookieFillingsView({cookie: this.cookie, cookieFillings: cookieFillings});

			this.showChildView('cookieFillings', cookieFillingsView);
			this.getRegion('cookieFillings').$el.show();
			this.getRegion('cookieFlavor').$el.hide();

			this.scrollWizardInView();
		},

		onCookieFillingsClickNext: function () {
			var cookieToppings = this.cookieData.findWhere({id: this.cookie.get('cookieType')}).attributes.toppings,
				cookieToppingsView,
				cookieResultView;
			if (cookieToppings) {
				cookieToppingsView = new CookieToppingsView({cookie: this.cookie, cookieToppings: cookieToppings});
				this.showChildView('cookieToppings', cookieToppingsView);
				this.getRegion('cookieToppings').$el.show();
			} else {
				cookieResultView = new CookieResultView({cookie: this.cookie});
				this.getRegion('cookieToppings').$el.hide();
				this.showChildView('cookieResult', cookieResultView);
				this.getRegion('cookieResult').$el.show();
			}
			this.getRegion('cookieFillings').$el.hide();

			this.scrollWizardInView();
		},

		onCookieToppingsClickNext: function () {
			var cookieResultView = new CookieResultView({cookie: this.cookie});
			this.showChildView('cookieResult', cookieResultView);
			this.getRegion('cookieResult').$el.show();
			this.getRegion('cookieToppings').$el.hide();

			this.scrollWizardInView();
		},

		onCookieResultClickDone: function () {
			this.trigger('wizard:nextStep');
		},

		onCookieFlavorClickPrevious: function () {
			this.getRegion('cookieFlavor').$el.hide();
			this.getRegion('cookieType').$el.show();

			this.scrollWizardInView();
		},

		onCookieFillingsClickPrevious: function () {
			this.getRegion('cookieFillings').$el.hide();
			this.getRegion('cookieFlavor').$el.show();

			this.scrollWizardInView();
		},

		onCookieToppingsClickPrevious: function () {
			this.getRegion('cookieToppings').$el.hide();
			this.getRegion('cookieFillings').$el.show();

			this.scrollWizardInView();
		},

		onCookieResultClickPrevious: function () {
			this.getRegion('cookieResult').$el.hide();
			if (this.cookie.get('cookieType') === 'milano-slices') {
				this.getRegion('cookieToppings').$el.show();
			} else {
				this.getRegion('cookieFillings').$el.show();
			}

			this.scrollWizardInView();
		},

		scrollWizardInView: function () {
			$('html, body').animate({
				scrollTop: $('#cookieWizard').offset().top + 100
			}, 200);
		},

		onRender: function () {
			var cookieTypesView = new CookieTypesView({cookie: this.cookie, collection: this.cookieData});
			this.showChildView('cookieType', cookieTypesView);
		}
	});

	return CookieWizardView;
});
