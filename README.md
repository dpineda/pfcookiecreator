![Codeship](https://codeship.com/projects/bce73e60-653e-0133-e6ce-4a7e5d8c8004/status?branch=master)
[![codebeat badge](https://codebeat.co/badges/9f4dc4ef-a132-4904-8ff7-2db95667250f)](https://codebeat.co/projects/bitbucket-org-dpineda-pfcookiecreator-master)

How to Run
=======

- Download and install [node](https://nodejs.org/en/download/)
- Install [grunt](http://gruntjs.com/getting-started)
- run `node install` inside the root project directory to install node dependencies
- run `grunt` to launch the project in development mode (without optimized and minified javascript).
- runt `grunt prod` to launch the project in production mode (rjs optimized and minified javascript).
- point your browser to `http://localhost:3000`


TODOs
======

- Refactor back, previous logic into cookieWizardLayoutView to DRY up code.
- Try out webpack packaging.
- Add more animations between the cookie steps.
- Refactor form validation into a reusable Marionette Behaviour.
- Add in the other parts of the app flow (profile, sign in).
- Look into fleshing out a backend using preferably solutions/frameworks in Javascript (node, express, sailsJs, etc).
